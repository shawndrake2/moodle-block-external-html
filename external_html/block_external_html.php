<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

class block_external_html extends block_base
{
    public function init() {
        $this->title = get_string('blocktitle', 'block_external_html');
    }

    public function specialization() {
        if (!empty($this->config->title)) {
            $this->title = $this->config->title;
        } else {
            $this->config->title = get_string('blocktitle', 'block_external_html');
        }

        if (empty($this->config->text)) {
            $this->config->text = get_string('blockstring', 'block_external_html');
        }
    }

    // Allow multiple instances of the block on a page.
    public function instance_allow_multiple() {
        return true;
    }

    // Choose whether to hide the header or not.
    public function hide_header() {
        return false;
    }

    // Enable CSS styling of the block.
    public function html_attributes() {
        $attributes = parent::html_attributes();  // Get default values
        $attributes['class'] .= ' block_'.$this->name();  // Append our class to class attribute.
        return $attributes;
    }

    // What pages can display this block?
    public function applicable_formats() {
        return array('site' => true);  // Any page.
    }

    // Get content of the block.
    public function get_content() {

        if ($this->content !== null) {
            return $this->content;
        }

        $this->content = new stdClass;
        $this->content->text = $this->get_content_text();
        $this->content->footer = $this->get_content_footer();

        return $this->content;
    }

    // Return the text that belongs given the external link.
    private function get_content_text() {
        $ch = curl_init();
        if (!empty($this->config->text)) {
            $link = $this->config->text;
        }
        curl_setopt($ch, CURLOPT_URL, $link);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $content = curl_exec($ch);
        curl_close($ch);

        return $content;
    }

    // No footer available.
    private function get_content_footer() {
        return '';
    }
}