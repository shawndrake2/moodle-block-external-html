Ext-HTML-Block_Moodle
=====================

Block designed for Moodle 2.4 that allows a user to 
link to an external HTML page that will display inside 
of a block in the Moodle LMS.

This is very raw plugin that requires the user to be
aware of the code he/she is sending to it. This block 
was a quick solution to the problem of third party hosting 
making it hard to update data. It's primary function was
simply to pass an html navigation pane through it.
